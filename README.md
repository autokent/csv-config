# csv-config

**A `CSV` config parser and serializer for node**

[![version](https://img.shields.io/npm/v/csv-config.svg)](https://www.npmjs.org/package/csv-config)
[![downloads](https://img.shields.io/npm/dt/csv-config.svg)](https://www.npmjs.org/package/csv-config)
[![node](https://img.shields.io/node/v/csv-config.svg)](https://nodejs.org/)
[![status](https://gitlab.com/autokent/csv-config/badges/master/pipeline.svg)](https://gitlab.com/autokent/csv-config/pipelines)

## Installation
`npm install csv-config`

## Usage

### Reading Config Files at Current Working Directory `cwd`
```js
const CsvConfig = require('csv-config');

let config = CsvConfig("CsvConfig",true);

//reading config values
let data01=config['conf-01'].data[0][0];
let data02=config['conf-02'].data[0][0];
let data03=config['conf-03'].data[0][0];

//updating
config['conf-01'].data[0][1] = 1;
config['conf-02'].data[0][1] = 2;
config['conf-03'].data[0][1] = 3;

//saving configs
config.save();
```

### Reading Config Files at User Home Directory
```js
const CsvConfig = require('csv-config');

let config = CsvConfig("CsvConfig");

//reading config values
let data01=config['conf-01'].data[0][0];
let data02=config['conf-02'].data[0][0];
let data03=config['conf-03'].data[0][0];

//updating
config['conf-01'].data[0][1] = 1;
config['conf-02'].data[0][1] = 2;
config['conf-03'].data[0][1] = 3;

//saving configs
config.save();
```

## Test
`mocha` or `npm test`

> check test folder and QUICKSTART.js for extra usage.