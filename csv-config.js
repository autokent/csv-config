const path = require('path');
const fs = require('fs-extra');
const userHome = require('user-home');
const papa = require('papaparse');
const CONFIG = {};

// create a unique, global symbol name
const OBJ_KEY = Symbol.for("csv-config");

// check if the global object has this symbol
// add it if it does not have the symbol, yet
var globalSymbols = Object.getOwnPropertySymbols(global);

function CsvConfig(folderName, islocal) {

	if (globalSymbols.indexOf(OBJ_KEY) < 0) {
		global[OBJ_KEY] = CONFIG;
	} else {
		CONFIG = global[OBJ_KEY];
		return CONFIG;
	}

	if (typeof folderName == "undefined") folderName = "csv-config";
	if (typeof islocal == "undefined") islocal = false;

	let cfgPathLocal = path.resolve(process.cwd(), folderName, "config");
	let cfgPathHome = path.resolve(userHome, folderName, "config");

	if (islocal) {
		CONFIG['current-config-dir'] = cfgPathLocal;
	} else {
		CONFIG['current-config-dir'] = cfgPathHome;

		if (!fs.pathExistsSync(cfgPathHome)) {
			fs.mkdirsSync(cfgPathHome);
			if (fs.pathExistsSync(cfgPathLocal)) {
				fs.copySync(cfgPathLocal, cfgPathHome, {
					overwrite: true
				});
			}
		}
	}

	let parse_config = {
		delimiter: ";",
		skipEmptyLines: true,
		header: false
	};

	let unparse_config = {
		quotes: false,
		quoteChar: '"',
		delimiter: ";",
		header: false,
		newline: "\r\n",
		header: false
	};

	var files = fs.readdirSync(CONFIG['current-config-dir']);

	for (var i in files) {
		let ext_name = path.extname(files[i]);
		if (ext_name === ".csv") {
			let base_name = path.basename(files[i], ext_name);
			let file_path = path.resolve(CONFIG['current-config-dir'], files[i]);
			let file_text = fs.readFileSync(file_path, 'utf8');

			if (/^((#[^;\n\r]+)[;]?)+/.test(file_text)) {
				parse_config.header = true;
			} else {
				parse_config.header = false;
			}

			CONFIG[base_name] = papa.parse(file_text, parse_config);
			CONFIG[base_name].header = parse_config.header;
		}
	}

	CONFIG['save'] = function () {
		for (const [key, value] of Object.entries(CONFIG)) {
			if (typeof (value) !== 'function' && key != 'current-config-dir') {

				unparse_config.header = CONFIG[key].header;

				let text = papa.unparse(CONFIG[key].data, unparse_config);
				let file_path = path.resolve(CONFIG['current-config-dir'], `${key}.csv`);
				fs.writeFileSync(file_path, text, 'utf8');
			}
		}
	};

	return CONFIG;
};

module.exports = CsvConfig;

//for testing purpose
if (!module.parent) {
	let config = CsvConfig("CsvConfig", true);
	debugger;
	//config['atwords'].data[0][1]++;
	//config['dotwords'].data[0][1]++;
	//config['blacklist'].data[0][1]++;
	//config.save();
}
