const assert = require('assert');
const CsvConfig = require("../");

describe('reading csv config at user home folder', function () {
	it('should read rows at csv file', function () {
		let config = CsvConfig("CsvConfig", false);

		let data01 = config['conf-01'].data[0][0];
		let data02 = config['conf-02'].data[0][0];
		let data03 = config['conf-03'].data[0][0];

		assert.equal(data01, "data-01_1");
		assert.equal(data02, "data-02_1");
		assert.equal(data03, "data-03_1");
	});
});
